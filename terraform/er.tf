provider "vultr" {
  api_key     = var.vultr_api_key
  rate_limit  = 700
  retry_limit = 3
}

variable "vultr_rs_chr_7_1_3_image_id" {
  type    = string
  default = "5988d8d0-f9f1-4c66-9fad-e4dc3540b482"
}

resource "vultr_instance" "er1_atl" {
  plan             = "vc2-1c-1gb"
  label            = "er1.atl"
  tag              = var.env
  region           = "atl"
  snapshot_id      = var.vultr_rs_chr_7_1_3_image_id
  hostname         = "er1.atl.${var.domain_prepend}${var.root_domain}"
  enable_ipv6      = false
  backups          = "disabled"
  ddos_protection  = false
  activation_email = false

  provisioner "local-exec" {
    command = "apk add --update openssh && ssh -o StrictHostKeyChecking=no -p 22 admin@${self.main_ip} '/user add name=hamwan group=full password=${var.ansible_ssh_secret};/user remove [find name=admin];/ip service set ssh port=222'"
  }
}

resource "powerdns_record" "vultr_er1_atl" {
  zone    = "${var.root_domain}."
  name    = "vultr.er1.atl.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = [vultr_instance.er1_atl.main_ip]
}

resource "powerdns_record" "vultr_er1_atl_cname" {
  zone    = "${var.root_domain}."
  name    = "er1.atl.${var.domain_prepend}${var.root_domain}."
  type    = "CNAME"
  ttl     = 300
  records = ["vultr.er1.atl.${var.domain_prepend}${var.root_domain}."]
}

resource "powerdns_record" "vultr_er1_bridge1" {
  zone    = "${var.root_domain}."
  name    = "bridge1.er1.atl.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.129.209"]
}
