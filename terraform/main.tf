terraform {
  backend "http" {

  }
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.1"
    }
    powerdns = {
      source = "pan-net/powerdns"
    }
    vultr = {
      source  = "vultr/vultr"
      version = "2.9.1"
    }
  }
}

variable "gitlab_access_token" { #there is a ci/cd var listed in gitlab corresponding with this name
  type = string
}
variable "powerdns_server_url" { #there is a ci/cd var listed in gitlab corresponding with this name
  type = string
}
variable "powerdns_api_key" { #there is a ci/cd var listed in gitlab corresponding with this name
  type = string
}
variable "vultr_api_key" { #there is a ci/cd var listed in gitlab corresponding with this name
  type = string
}
variable "ansible_ssh_secret" { #there is a ci/cd var listed in gitlab corresponding with this name
  type = string
}
variable "ssh_public_key" { #there is a ci/cd var listed in gitlab corresponding with this name
  type = string
}

variable "root_domain" {
  type        = string
  description = "Root domain in which things are running"
  default     = "memhamwan.local"
}

variable "only_in_production" {
  type        = number
  description = "1 if production, otherwise 0"
  default     = 0
}


variable "domain_prepend" {
  type        = string
  description = "Prepended domain segment as needed for the dev environment"
  default     = ""
}

variable "env" {
  type        = string
  description = "Environment being executed; so far, this is either local or production"
  default     = "local"
}

provider "gitlab" {
  token = var.gitlab_access_token
}

data "gitlab_project" "example_project" {
  id = 29828383 #this is taken from https://gitlab.com/memhamwan/ioc-terraform
}
