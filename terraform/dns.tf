provider "powerdns" {
  server_url = var.powerdns_server_url
  api_key    = var.powerdns_api_key
}

resource "powerdns_record" "foobar" {
  zone    = "${var.root_domain}."
  name    = "www.${var.domain_prepend}${var.root_domain}."
  type    = "CNAME"
  ttl     = 300
  records = ["ingress.k8s.${var.domain_prepend}${var.root_domain}."]
}

resource "powerdns_record" "m17" {
  zone    = "${var.root_domain}."
  name    = "m17.${var.domain_prepend}${var.root_domain}."
  type    = "CNAME"
  ttl     = 300
  records = ["ingress.k8s.${var.domain_prepend}${var.root_domain}."]
}

resource "powerdns_record" "memhamwan_net_root" {
  zone    = "${var.root_domain}."
  name    = "${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.132.8"]
}


resource "powerdns_record" "r3_leb" {
  zone    = "${var.root_domain}."
  name    = "r3.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.163"]
}

resource "powerdns_record" "durian_leb" {
  zone    = "${var.root_domain}."
  name    = "durian.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.165"]
}

resource "powerdns_record" "aprs_sco_" {
  zone    = "${var.root_domain}."
  name    = "aprs.sco.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.128.44"]
}

resource "powerdns_record" "wildcard_durian_leb" {
  zone    = "${var.root_domain}."
  name    = "*.durian.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.165"]
}

resource "powerdns_record" "ilo_durian_leb" {
  zone    = "${var.root_domain}."
  name    = "ilo.durian.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.166"]
}

resource "powerdns_record" "adsb" {
  zone    = "${var.root_domain}."
  name    = "adsb.${var.domain_prepend}${var.root_domain}."
  type    = "CNAME"
  ttl     = 300
  records = ["adsb.sco.${var.domain_prepend}${var.root_domain}."]
}

resource "powerdns_record" "adsb_sco" {
  zone    = "${var.root_domain}."
  name    = "adsb.sco.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.42"]
}

resource "powerdns_record" "stream_sco" {
  zone    = "${var.root_domain}."
  name    = "stream.sco.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.37"]
}

resource "powerdns_record" "dns_leb" {
  zone    = "${var.root_domain}."
  name    = "dns.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.177"]
}

resource "powerdns_record" "ntp_leb" {
  zone    = "${var.root_domain}."
  name    = "ntp.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.181"]
}

resource "powerdns_record" "prometheus" {
  zone    = "${var.root_domain}."
  name    = "prometheus.${var.domain_prepend}${var.root_domain}."
  type    = "CNAME"
  ttl     = 300
  records = ["ingress.k8s.${var.domain_prepend}${var.root_domain}."]
}
resource "powerdns_record" "grafana" {
  zone    = "${var.root_domain}."
  name    = "grafana.${var.domain_prepend}${var.root_domain}."
  type    = "CNAME"
  ttl     = 300
  records = ["ingress.k8s.${var.domain_prepend}${var.root_domain}."]
}
resource "powerdns_record" "alertmanager" {
  zone    = "${var.root_domain}."
  name    = "alertmanager.${var.domain_prepend}${var.root_domain}."
  type    = "CNAME"
  ttl     = 300
  records = ["ingress.k8s.${var.domain_prepend}${var.root_domain}."]
}
resource "powerdns_record" "dashy" {
  zone    = "${var.root_domain}."
  name    = "dashy.${var.domain_prepend}${var.root_domain}."
  type    = "CNAME"
  ttl     = 300
  records = ["ingress.k8s.${var.domain_prepend}${var.root_domain}."]
}

resource "powerdns_record" "vrrp_ftn" {
  zone    = "${var.root_domain}."
  name    = "vrrp.ftn.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.129.193"]
}

resource "powerdns_record" "r1_ftn" {
  zone    = "${var.root_domain}."
  name    = "r1.ftn.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.194"]
}

resource "powerdns_record" "sco_ftn" {
  zone    = "${var.root_domain}."
  name    = "sco.ftn.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.195"]
}

resource "powerdns_record" "s14545_ftn" {
  zone    = "${var.root_domain}."
  name    = "wb4kog45.ftn.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.197"]
}

resource "powerdns_record" "er1_ret" {
  zone    = "${var.root_domain}."
  name    = "er1.ret.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.129.162"]
}

resource "powerdns_record" "wlan1_leb_ret" {
  zone    = "${var.root_domain}."
  name    = "wlan1.leb.ret.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.131.146"]
}
resource "powerdns_record" "wlan1_ret_leb" {
  zone    = "${var.root_domain}."
  name    = "wlan1.ret.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.131.147"]
}

resource "powerdns_record" "ether1_leb_ret" {
  zone    = "${var.root_domain}."
  name    = "ether1.leb.ret.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.129.161"]
}

resource "powerdns_record" "ap_leb" {
  zone    = "${var.root_domain}."
  name    = "ap.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.188"]
}

resource "powerdns_record" "ap_ups" {
  zone    = "${var.root_domain}."
  name    = "ups.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.128.189"]
}

resource "powerdns_record" "sstp_leb_ret" {
  zone    = "${var.root_domain}."
  name    = "sstp.leb.ret.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.131.165"]
}

resource "powerdns_record" "sstp_lebret_er1" {
  zone    = "${var.root_domain}."
  name    = "sstp-lebret.er1.atl.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  records = ["44.34.131.164"]
}

resource "powerdns_record" "sstp_ap_leb" {
  zone    = "${var.root_domain}."
  name    = "sstp.ap.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  set_ptr = true
  records = ["44.34.131.167"]
}

resource "powerdns_record" "sstp_apleb_er1" {
  zone    = "${var.root_domain}."
  name    = "sstp-apleb.er1.atl.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.131.166"]
}


resource "powerdns_record" "sstp_oob_ftn_remote" {
  zone    = "${var.root_domain}."
  name    = "oob.r1.ftn.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  set_ptr = true
  records = ["44.34.131.169"]
}

resource "powerdns_record" "sstp_oob_ftn_local" {
  zone    = "${var.root_domain}."
  name    = "oob-ftn.er1.atl.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.131.168"]
}

resource "powerdns_record" "ap1_mno" {
  zone    = "${var.root_domain}."
  name    = "ap1.mno.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  ttl     = 300
  set_ptr = true
  records = ["44.34.129.49"]
}

resource "powerdns_record" "lte_sco" {
  zone    = "${var.root_domain}."
  name    = "lte.sco.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.128.38"]
}


resource "powerdns_record" "ref067_leb" {
  zone    = "${var.root_domain}."
  name    = "ref067.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.128.170"]
}

resource "powerdns_record" "allstar_leb" {
  zone    = "${var.root_domain}."
  name    = "allstar.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.128.178"]
}

resource "powerdns_record" "aprs_floating" {
  zone    = "${var.root_domain}."
  name    = "aprs.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = false # Can't do this because 44.34.132 doesnt exist for ptrs currently :/ need to do that step manually
  ttl     = 300
  records = ["44.34.132.7"]
}

resource "powerdns_record" "ingress_k8s_floating" {
  zone    = "${var.root_domain}."
  name    = "ingress.k8s.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = false # Can't do this because 44.34.132 doesnt exist for ptrs currently :/ need to do that step manually
  ttl     = 300
  records = ["44.34.132.8"]
}

resource "powerdns_record" "drats_floating" {
  zone    = "${var.root_domain}."
  name    = "drats.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = false # Can't do this because 44.34.132 doesnt exist for ptrs currently :/ need to do that step manually
  ttl     = 300
  records = ["44.34.132.18"]
}

resource "powerdns_record" "ingress_k8s_floating_wildcard" {
  zone    = "${var.root_domain}."
  name    = "*.ingress.k8s.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = false # Can't do this because 44.34.132 doesnt exist for ptrs currently :/ need to do that step manually
  ttl     = 300
  records = ["44.34.132.8"]
}

resource "powerdns_record" "esxi_leb" {
  zone    = "${var.root_domain}."
  name    = "esxi1.leb.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.128.164"]
}

resource "powerdns_record" "dstar_wb4kog" {
  zone    = "${var.root_domain}."
  name    = "dstar-wb4kog.end-user.hil.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.128.98"]
}

resource "powerdns_record" "r4_azo" {
  zone    = "${var.root_domain}."
  name    = "r4.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.75"]
}

resource "powerdns_record" "r3_azo" {
  zone    = "${var.root_domain}."
  name    = "r3.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.74"]
}

resource "powerdns_record" "r2_azo" {
  zone    = "${var.root_domain}."
  name    = "r2.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.73"]
}

resource "powerdns_record" "r1_azo" {
  zone    = "${var.root_domain}."
  name    = "r1.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.66"]
}

resource "powerdns_record" "vrrp_azo" {
  zone    = "${var.root_domain}."
  name    = "vrrp.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.65"]
}

resource "powerdns_record" "leb_azo" {
  zone    = "${var.root_domain}."
  name    = "leb.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.68"]
}


resource "powerdns_record" "wireless_leb_azo" {
  zone    = "${var.root_domain}."
  name    = "wlan1.leb.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = false
  ttl     = 300
  records = ["44.34.131.143"]
}

resource "powerdns_record" "omn2_azo" {
  zone    = "${var.root_domain}."
  name    = "omn2.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.67"]
}

resource "powerdns_record" "cam1_azo" {
  zone    = "${var.root_domain}."
  name    = "cam1.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.72"]
}

resource "powerdns_record" "weather_azo" {
  zone    = "${var.root_domain}."
  name    = "weather.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.71"]
}

resource "powerdns_record" "ups_azo" {
  zone    = "${var.root_domain}."
  name    = "ups.azo.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.77"]
}
resource "powerdns_record" "vrrp_crw" {
  zone    = "${var.root_domain}."
  name    = "vrrp.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.113"]
}
resource "powerdns_record" "omn1_crw" {
  zone    = "${var.root_domain}."
  name    = "omn1.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.114"]
}
resource "powerdns_record" "uplink_crw" {
  zone    = "${var.root_domain}."
  name    = "uplink.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.115"]
}
resource "powerdns_record" "r1_crw" {
  zone    = "${var.root_domain}."
  name    = "r1.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.116"]
}
resource "powerdns_record" "r2_crw" {
  zone    = "${var.root_domain}."
  name    = "r2.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.117"]
}
resource "powerdns_record" "r3_crw" {
  zone    = "${var.root_domain}."
  name    = "r3.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.118"]
}
resource "powerdns_record" "ups_crw" {
  zone    = "${var.root_domain}."
  name    = "ups.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.120"]
}
resource "powerdns_record" "ilo_coconut_crw" {
  zone    = "${var.root_domain}."
  name    = "ilo.coconut.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.124"]
}
resource "powerdns_record" "coconut_crw" {
  zone    = "${var.root_domain}."
  name    = "coconut.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.125"]
}
resource "powerdns_record" "cam_r2_crw" {
  zone    = "${var.root_domain}."
  name    = "cam.r2.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.145"]
}
resource "powerdns_record" "cam1_crw" {
  zone    = "${var.root_domain}."
  name    = "cam1.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.146"]
}
resource "powerdns_record" "cam2_crw" {
  zone    = "${var.root_domain}."
  name    = "cam2.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.147"]
}
resource "powerdns_record" "cam3_crw" {
  zone    = "${var.root_domain}."
  name    = "cam3.crw.${var.domain_prepend}${var.root_domain}."
  type    = "A"
  set_ptr = true
  ttl     = 300
  records = ["44.34.129.148"]
}
