resource "vultr_dns_domain" "memhamwan_org" {
  count  = var.only_in_production
  domain = "memhamwan.org"
}
resource "vultr_dns_record" "memhamwan_org_www" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = "www"
  data   = "www.memhamwan.net"
  type   = "CNAME"
}
resource "vultr_dns_record" "memhamwan_org_email_cname" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = "email"
  data   = "mailgun.org"
  type   = "CNAME"
}

resource "vultr_dns_record" "memhamwan_org_root" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = ""
  data   = "44.34.132.8"
  type   = "A"
}

resource "vultr_dns_record" "memhamwan_org_gitlab_pages_verification" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = "_gitlab-pages-verification-code"
  data   = "gitlab-pages-verification-code=8c27c895c29d3a127a0757898eb99b4a"
  type   = "TXT"
}

resource "vultr_dns_record" "memhamwan_org_gitlab_pages_verification_www" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = "_gitlab-pages-verification-code.www"
  data   = "gitlab-pages-verification-code=442f716e09cb45f8fa9503bede45b90b"
  type   = "TXT"
}

resource "vultr_dns_record" "memhamwan_org_caa" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = ""
  data   = "0 issue \"letsencrypt.org\""
  type   = "CAA"
}

resource "vultr_dns_record" "memhamwan_org_spf" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = ""
  data   = "\"v=spf1 include:_spf.google.com ~all\""
  type   = "TXT"
}

resource "vultr_dns_record" "memhamwan_org_mailgun_spf" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = ""
  data   = "\"v=spf1 include:mailgun.org ~all\""
  type   = "TXT"
}

resource "vultr_dns_record" "memhamwan_org_google_mailgun_domainkey" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = "k1._domainkey"
  data   = "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjlcshZdMr2VyOM/3nN9fa1ZfA3qBe9VvgMS4ACYIKjcxpscCA47z/zqh8HlKOpvcIOdnJgTlWOv5AnPVSrnPpYTb2ceuTkrZSKVujOzfdiGbqR8TPMFnzvloEHWg8xR18jUz2Ruh1FeJUrq45+sy6GvjDYE14JJ6LioP7orvpQwIDAQAB"
  type   = "TXT"
}

resource "vultr_dns_record" "memhamwan_org_google_domainkey" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = "google._domainkey"
  data   = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp/af1ngxXXWuByh0nvHRPc7iJ8JxLGVKw2b6qWGz2KYnLAcUU2fKRXBHydi1MHXQr/4/QussGGWKyodP7Q4kU/AwvGZQuGYuL11MjFUHyl/+83w7v6ND4PB/IS8mO6B14p22mElq22Jo8j3Ds2wdgwWVJ0xaijdlcxYr0wMRHJ4PugewVXgOOmMNFQJ7lFQaCT8yBjpkTXvxvX73bSpEBcX3mpcz6iy9n0HRe3PreJpXi4dbsLbDg1XP2oh6DROWbq4Mg6abP98sF+9dnod9u+PkZrq8fXtWSer0unaBGLmfQWTmmcYK8wrzWikqzmLWyYPt4DFXniQ3M/zdUqxNQwIDAQAB"
  type   = "TXT"
}

resource "vultr_dns_record" "memhamwan_org_google_search_console" {
  count  = var.only_in_production
  domain = vultr_dns_domain.memhamwan_org[0].id
  name   = ""
  data   = "google-site-verification=Bvyi8wkmbcbcb388iR4-yL81kym-y8_ngVDmavxoIzE"
  type   = "TXT"
}

resource "vultr_dns_record" "memhamwan_org_mx1" {
  count    = var.only_in_production
  domain   = vultr_dns_domain.memhamwan_org[0].id
  name     = ""
  data     = "aspmx.l.google.com"
  priority = "1"
  type     = "MX"
}

resource "vultr_dns_record" "memhamwan_org_mx5a" {
  count    = var.only_in_production
  domain   = vultr_dns_domain.memhamwan_org[0].id
  name     = ""
  data     = "alt1.aspmx.l.google.com"
  priority = "5"
  type     = "MX"
}

resource "vultr_dns_record" "memhamwan_org_mx5b" {
  count    = var.only_in_production
  domain   = vultr_dns_domain.memhamwan_org[0].id
  name     = ""
  data     = "alt2.aspmx.l.google.com"
  priority = "5"
  type     = "MX"
}

resource "vultr_dns_record" "memhamwan_org_mx10a" {
  count    = var.only_in_production
  domain   = vultr_dns_domain.memhamwan_org[0].id
  name     = ""
  data     = "alt3.aspmx.l.google.com"
  priority = "10"
  type     = "MX"
}

resource "vultr_dns_record" "memhamwan_org_mx10b" {
  count    = var.only_in_production
  domain   = vultr_dns_domain.memhamwan_org[0].id
  name     = ""
  data     = "alt4.aspmx.l.google.com"
  priority = "10"
  type     = "MX"
}