# Infra as Code

This repository contains infrastructure as code for HamWAN Memphis Metro. It uses a combination of technologies and is intended to be used primarily end-to-end, meaning that while these scripts can be executed locally, the emphasis is on creating a robust set of infrastructure code that can be delivered using CICD pipelines.

## Frequent Actions

### Adding an OpenVPN User

1. In GitLab's CI configuration, add a new entry to the "er_ppp_users" variable
2. Manually run the pipeline
3. Tell the user to use the configuration accessible in `ansible/roles/edge_routers/files/config.ovpn`

### Add a new netop user

1. Get their SSH public key, set the name to the username, and add it to `ansible/files/`
2. Add the username `ansible/group_vars/all.yaml` under the `netops_users` var
3. Commit these changes and let the automation run on the default branch; the user will be added to all ssh managed devices

## Workflow

The `main` branch represents our current latest version. Anything merged into that should be executed automatically via CD.

## Scope

- Today, this is in use "for real" to manage adding new DNS records for the memhamwan.net domain; not all records are defined here, but if you're making something new, you should do it here; this is achieved using terraform
- The first appliance devices being managed by this repo are the cloud edge routers; this is achieved using terraform and ansible

## Setting up local environment

### Ansible

Nothing special needed, though look under `ansible/group_vars` for various secrets that are expected to be on the environment. This approach was chosen over ansible vaults because our repository is primarily intended for execution via CICD, and managing secrets in the CICD configuration is simpler.

Make sure you have python package dnspython installed.

### Terraform

Terraform has the concept of "state" which represents... the state of an environment. Currently, we have one named `default` which is for production. Generally you should not manually be managing things that are recorded in state, but given the dynamic nature of our environment, that's not always practical. In case you need to manually manage state or run some things ad-hoc, the shared state.

1. Create a personal access token in GitLab; export this in your `.profile` or some place as `GITLAB_ACCESS_TOKEN`
2. Cruise over to [infrastructure->terraform](https://gitlab.com/memhamwan/ioc-terraform/-/terraform), look for default (assuming you need prod), and click the more button on the right; select "Copy Terraform init command"
3. CD to the terraform root directory of this repo, which is `terraform`
4. Run the command from step 2, though be sure you skip the first line since you already took care of this in step 1

## Prerequisites

### Vultr provider

For the vultr account used for the various cloud appliances, it's expected that there will be a snapshot on the account with a routeros v7 install. This is necessary because routeros is not an officially supported OS, so at some point a base installation has to be done. This image is simply the raw image from mikrotik's website uploaded as a "snapshot". The file is present under `/terraform/files` A TODO around this is to script uploading the snapshot directly from here in terraform so that this disclaimer isn't needed at all...
