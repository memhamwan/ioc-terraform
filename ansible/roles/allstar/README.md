# Allstar Role

## Extra stuff

- https://n8acl.github.io/posts/bridge-allstar-to-discord
- https://wiki.m17.link/usrp2m17_bridge

## Dependencies

- Outside of this repo you are running the discord bot needed for the allstar-discord bridge; the IP and port for that are passed in the variable 

## Vars

- discord_bridge_remote_ip
- discord_bridge_remote_port
