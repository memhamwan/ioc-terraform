import sys
import re

print("\nProcessing file: ", sys.argv[1])

filename = sys.argv[1]
lines = []

with open(filename, 'r') as fp:
    lines = fp.readlines()
filtered = list(filter(lambda x: not re.match(r'^\s*$', x), lines))

if filtered[0] == "/\n":
    filtered.pop(0)

filtered[0] = re.sub("^# [a-z]{3}/[0-9]*/[0-9]* [0-9]{2}:[0-9]{2}:[0-9]{2} by ", "# ", filtered[0])

with open(filename, 'w') as fp:
    for number, line in enumerate(filtered):
        fp.write(line)
