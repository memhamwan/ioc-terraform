# Readme

## Road Warrior VPN Tasks

Locally, SSL keys for an openvpn server CA have to be generated. These are important to be set outside of the automation because they need to be embedded also in the client openvpn configuration, which requires manual user adoption.

### Client Configuration

Refer to `files/config.ovpn` to import to your device. Note this is not suitable for a point-to-point and it is set to route *all* traffic over the tunnel, not just HamWAN or 44-net traffic. So please be courteous about the bandwidth you use and remember to observe good internet citizenry. We have to pay for this bandwidth by the GB and if you abuse this, your access will be cut!

### Adding a new OVPN user

See the in the root folder.

### Generating CA keys

Generate the cert on a mikrotik device, then export it. Make sure to note the passphrase. You'll need to scp the resulting file from the root of the router to your local after that. Assuming the file is named ca.crt... `ca.crt` at this point can be used to embed in `files/config.ovpn` under the `<ca></ca>` section. It is also important that `ca.crt` itself is put in the `files` directory and that the contents of `ca.key` are set in the Gitlab CI variable section under the `er_ovpn_ca_key` key (this should already exist, and you'll just need to update the value). Also, set the `er_ovpn_ca_key_passphrase` value accordingly. Remember that this key is a secret and that it should not be checked in anywhere.
TODO add details of server cert generation